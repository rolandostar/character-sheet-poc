const fp = require('fastify-plugin');
const { fileLoader } = require('merge-graphql-schemas');
const { basename, posix, join } = require('path');
const globby = require('globby');
const chalk = require('chalk');

function gqlStitch(fastify, options, done) {
  const gql = { schemas: [], resolvers: [] };
  fastify
    .decorate('importModels', (scanDir, instance, label) => {
      const db = [];
      instance.options.logging(chalk.yellow(`Loading ${label} models...`));
      const matchedModels = globby.sync([posix.join(scanDir, '/models/**/*.js')]);
      matchedModels.forEach((modelPath) => {
        instance.options.logging(`\t${chalk.white('import')}\t${basename(modelPath)}`);
        const model = instance.import(modelPath);
        db[model.name] = model;
      });
      const modelStorage = { ...db, ...instance.models };
      Object.keys(db).forEach((modelName) => {
        instance.options.logging(`\t${chalk.white('link')}\t${modelName}`);
        if ('associate' in db[modelName]) db[modelName].associate(modelStorage);
      });
    })
    .decorate('importGQL', (dirName) => {
      gql.schemas.push(...fileLoader(join(dirName, '/schema/**/*.gql')));
      gql.resolvers.push(...fileLoader(join(dirName, '/resolvers/**/*.resolver.js')));
    });
  done(gql);
}

module.exports = fp(gqlStitch);
