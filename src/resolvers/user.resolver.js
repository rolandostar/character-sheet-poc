const { resolver } = require('graphql-sequelize');
const { EXPECTED_OPTIONS_KEY } = require('dataloader-sequelize');

resolver.contextToOptions = { [EXPECTED_OPTIONS_KEY]: EXPECTED_OPTIONS_KEY };

module.exports = {
  Query: {
    getUsers: resolver((root, args, context) => context.models.User),
  },
  Mutation: {
    addUser: async (root, args, context) => {
      const { name } = args;
      const { User } = context.models;

      let user = await User.findOne({
        where: { name },
      });
      if (user) {
        return {
          code: 400,
          success: false,
          message: 'User already exists',
        };
      }
      if (!user) user = await User.create({ name });
      return {
        code: 200,
        success: true,
        message: 'User has been created',
        user,
      };
    },
  },
};
