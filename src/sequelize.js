const fp = require('fastify-plugin');
const Sequelize = require('sequelize');
const config = require('config');
const cls = require('cls-hooked');

const cfg = { ...config.get('database') };
const namespace = cls.createNamespace('sequelizeCLS');
Sequelize.useCLS(namespace);

function fastifySequelize(fastify, options, done) {
  const app = fastify;
  // Enable closing hook
  fastify.addHook('onClose', (f, cb) => {
    f.sequelize.close().then(() => {
      f.log.info('DATABASE\t[%s]', f.chalk.green('DISCONNECTED'));
      cb();
    });
  });

  // Connect to Database
  fastify.decorate(
    'sequelize',
    cfg.use_env_variable
      ? new Sequelize(process.env[cfg.use_env_variable], cfg)
      : new Sequelize(cfg.database, cfg.username, cfg.password, cfg),
  );

  if (options.log) app.sequelize.options.logging = (msg) => fastify.log.trace(msg);
  else app.sequelize.options.logging = false;
  done();
}

module.exports = fp(fastifySequelize);
