const fastify = require('./server');

const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || '0.0.0.0';

fastify
  .ready()
  .then(() => {
    fastify.log.info('NODE_ENV\t[%s]', fastify.chalk.green(process.env.NODE_ENV.toUpperCase()));
    fastify.log.info('SERVICE \t[%s]', fastify.chalk.green('READY'));
    fastify.listen(PORT, HOST).then(() => {
      fastify.log.trace(fastify.printRoutes());
    });
  })
  .catch((err) => {
    fastify.log.error('SERVER\t[ERROR]\n%s', err.message);
    process.exit(1);
  });
