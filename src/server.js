const config = require('config');
const { basename, posix, join } = require('path');
const globby = require('globby');
const { createContext, EXPECTED_OPTIONS_KEY } = require('dataloader-sequelize');
const fastify = require('fastify')({ logger: { ...config.get('logger') } });
const { mergeTypes, mergeResolvers, fileLoader } = require('merge-graphql-schemas');
const { makeExecutableSchema } = require('graphql-tools');
const GQL = require('fastify-gql');

const gql = { schemas: [], resolvers: [] };

fastify
  .register(require('fastify-chalk'), { disabled: true })
  .after(() => {
    fastify
      .decorate('importModels', (scanDir, instance, label) => {
        const db = [];
        instance.options.logging(fastify.chalk.yellow(`Loading ${label} models...`));
        const matchedModels = globby.sync([posix.join(scanDir, '/models/**/*.js')]);
        matchedModels.forEach((modelPath) => {
          instance.options.logging(`\t${fastify.chalk.white('import')}\t${basename(modelPath)}`);
          const model = instance.import(modelPath);
          db[model.name] = model;
        });
        const modelStorage = { ...db, ...instance.models };
        Object.keys(db).forEach((modelName) => {
          instance.options.logging(`\t${fastify.chalk.white('link')}\t${modelName}`);
          if ('associate' in db[modelName]) db[modelName].associate(modelStorage);
        });
      })
      .decorate('importGQL', (dirName) => {
        gql.schemas.push(...fileLoader(join(dirName, '/schema/**/*.gql')));
        gql.resolvers.push(...fileLoader(join(dirName, '/resolvers/**/*.resolver.js')));
      });
  })
  .register(require('fastify-helmet'))
  .register(require('fastify-formbody'))
  .register(require('./sequelize'), config.get('sequelize'))
  .after(async () => {
    fastify.log.debug('Attempting database connection...');
    try { await fastify.sequelize.authenticate(); } catch (err) {
      fastify.log.error('DATABASE\t[CONNECTION ERROR]\n%s', err.message);
      process.exit(1);
    }
    fastify.log.info('DATABASE\t[%s]', fastify.chalk.green('CONNECTED'));
    // fastify.sequelize.sync({ force: true });
    fastify.importModels(__dirname, fastify.sequelize, 'System');
    fastify.importGQL(__dirname);
    fastify.register(GQL, {
      schema: makeExecutableSchema({
        typeDefs: mergeTypes(gql.schemas),
        resolvers: mergeResolvers(gql.resolvers),
        // schemaDirectives, // TODO: Enable per module too
        resolverValidationOptions: { requireResolversForResolveType: false },
      }),
      // tracing: true,
      path: '/api',
      // jit: 1,
      // subscription: true,
      graphiql: 'playground',
      // errorHandler: (err, request, reply) => {
      //   request.log.warn(err)
      // },
      // errorHandler: true,
      context: (request, reply) =>
        // const dataloaderContext =
        ({
          models: fastify.sequelize.models,
          // app: fastify,
          // request,
          // reply,
          // user: { ...request.session.get('userData') },
          [EXPECTED_OPTIONS_KEY]: createContext(fastify.sequelize),
        })
      ,
    });
  });

// fastify.post('/', (request, reply) => {
//   fastify.log.debug(request.body);
//   reply.send(request.body);
// });

module.exports = fastify;
