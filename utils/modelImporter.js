const { basename, posix } = require('path');
const globby = require('globby');
const chalk = require('chalk');

module.exports.importModels = (scanDir, instance, label) => {
  const db = [];
  instance.options.logging(chalk.yellow(`Loading ${label} models...`));
  const matchedModels = globby.sync([posix.join(scanDir, '/models/**/*.js')]);
  matchedModels.forEach((modelPath) => {
    instance.options.logging(`\t${chalk.white('import')}\t${basename(modelPath)}`);
    const model = instance.import(modelPath);
    db[model.name] = model;
  });
  const modelStorage = { ...db, ...instance.models };
  Object.keys(db).forEach((modelName) => {
    instance.options.logging(`\t${chalk.white('link')}\t${modelName}`);
    if ('associate' in db[modelName]) db[modelName].associate(modelStorage);
  });
};
