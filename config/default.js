require('dotenv-safe').config();
const prettifier = require('pino-cone/pretty');
const sequelizerc = require('../.sequelizerc');

module.exports = {
  database: sequelizerc[process.env.NODE_ENV],
  logger: {
    redact: ['req.headers.authorization'],
    prettyPrint: true,
    prettifier,
    level: 'trace',
    base: null,
  },
  sequelize: { log: true },
};
